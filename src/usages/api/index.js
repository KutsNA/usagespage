import axios from 'axios';

const apiRoot = 'http://localhost:8000/measures';

export function findUsagesByDate(date) {
    return axios
        .get(`${apiRoot}/${date}`)
        .then(response => response.data);
}

export function findUsagesByMeasure(date, measure) {
    return axios
        //.get(`${apiRoot}/${date}/${measure}`)
        .get(`${apiRoot}/${measure}`)
        .then(response => response.data);
}

export function findUsagesByLimitName(date, name) {
    return axios
        .get(`${apiRoot}/byLimitName/${date}/${name}`)
        .then(response => response.data);
}