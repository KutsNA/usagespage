import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

class DialogWindowTable extends React.Component {
    state = {
        open: false
    };

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClickClose = () => {
        this.setState({open: false});
    };

    render() {
        const fields = Object.keys(this.props.fields).map(field => (
            <TableRow key={field}>
                <TableCell>
                    <DialogContent>
                        <DialogContentText>{field} : </DialogContentText>
                    </DialogContent>
                </TableCell>
                <TableCell>
                    <DialogContent>
                        <DialogContentText style={{color: 'black', textAlign: 'right'}}>{this.props.fields[field]}</DialogContentText>
                    </DialogContent>
                </TableCell>
            </TableRow>));
        return (
            <div>
                <Button onClick={this.handleClickOpen}>{this.props.fields.name}</Button>
                <Dialog open={this.state.open} onClose={this.handleClickClose} maxWidth='md'>
                    <Table>
                        <TableBody>
                            {fields}
                        </TableBody>
                    </Table>
                </Dialog>
            </div>
        );
    };
};

export default DialogWindowTable;