import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';


class DialogWindow extends React.Component {
    state = {
        open: false
    };

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClickClose = () => {
        this.setState({open: false});
    };

    render() {
        const fields = Object.keys(this.props.fields).map(field => (
            <ListItem key={field}>
                <DialogContent>
                    <DialogContentText>{field} : {this.props.fields[field]}</DialogContentText>
                </DialogContent>
            </ListItem>));
        return (
            <div>
                <Button onClick={this.handleClickOpen}>{this.props.fields.name}</Button>
                <Dialog open={this.state.open} onClose={this.handleClickClose} maxWidth='md'>
                    <List>
                        {fields}
                    </List>
                </Dialog>
            </div>
        );
    };
};

export default DialogWindow;