import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import ToolTip from '@material-ui/core/Tooltip';
import TableSortLable from '@material-ui/core/TableSortLabel'
import Paper from '@material-ui/core/Paper';
import DialogWindow from './DialogWindow';
import DialogWindowTable from './DialogWindowTable';

const colunmData = [
    {id: 'name', numeric: false, disablePadding: true, label: 'Limit name'},
    {id:'measure', numeric: false, disablePadding: false, label:'Measure'},
    {id:'usage', numeric: true, disablePadding: false, label:'usage'},
    {id:'min', numeric: true, disablePadding: false, label:'min'},
    {id:'max', numeric: true, disablePadding: false, label:'max'}
];

class SortableTableHead extends Component{

    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render(){
        const {order, orderBy }=this.props;
        return(
            <TableHead>
                <TableRow>
                        {colunmData.map(column => {
                            return(
                                <TableCell key={column.id} numeric={column.numeric} sortDirection={orderBy === column.id ? order : false}>
                                    <ToolTip title='Sort' placement={column.numeric? 'bottom-end' : 'bottom-start'} enterDelay={300}>
                                        <TableSortLable active={orderBy === column.id} direction={order} onClick={this.createSortHandler(column.id)}>
                                            {column.label}
                                        </TableSortLable>
                                    </ToolTip>
                                </TableCell>
                            );
                        }, this)}
                </TableRow>
            </TableHead>
        );
    };
}

SortableTableHead.propTypes={
    onRequestSort: PropTypes.func.isRequired,
    oreder: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired
};

let counter = 0;
function createData(name, measure, usage, min, max) {
    counter++;
    return{id: counter, name, measure, usage, min, max};
}

class SortableTable extends Component{
    constructor(props, context){
        super(props,context);

        this.state = {
            order: 'asc',
            orderBy: 'name',
            data: [
                createData('FI_LINE_VAR',       'DRILL', 12220,      0, 100),
                createData('REATES_LINE_VAR',   'VAR', 1245150,    -100, 200),
                createData('FI_LINE_VAR',       'OCP', 142414,     -200, 300),
                createData('FI_LINE_VAR',       'PV', 100500,     -300, 400),
                createData('FP_LINE_VAR',       'VAR', 0,          -400, 500),
                createData('CVA_LINE_VAR',      'VAR', 0,          -500, 600),
                createData('FP_LINE_VAR',       'DELTA', 0,          -600, 700),
                createData('CVA_VAR',           'VAR', 0,          -700, 800),
                createData('REATES_LINE_VAR',   'VAR', 800,         666, 900),
                createData('REATES_LINE_VAR',   'VAR', 900,         666, 900),
                createData('REATES_LINE_VAR',   'Base', 901,         666, 900),
                createData('REATES_LINE_VAR',   'VAR', 902,         666, 900),
                createData('REATES_LINE_VAR',   'PV', 903,         666, 900),
                createData('REATES_LINE_VAR',   'VAR', 904,         666, 900),
                createData('REATES_LINE_VAR',   'DELTA', 904,         666, 900),
                createData('REATES_LINE_VAR',   'BASE', 904,         666, 900),
                createData('REATES_LINE_VAR',   'CS01', 904,         666, 900),
            ],
            page: 0,
            rowsPerPage: 10
        };
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if(this.state.orderBy === property && this.state.order ==='desc')
            order = 'asc';

        const data = order === 'desc' ?
            this.state.data.sort((a,b) => (b[orderBy] < a[orderBy] ? -1 : 1)) :
            this.state.data.sort((a,b) => (a[orderBy] < b[orderBy] ? -1 : 1));

        this.setState({data, order, orderBy});
    };

    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    handleClose = () => {
        this.props.onClose();
    };

    render(){
        const {data, order, orderBy, rowsPerPage, page} = this.state;
        const dataFromNode = this.props.usages.map(el =>{
            createData(el[0], el[1], el[2], el[3], el[4]);
        } );
        return(
            <Paper>
                <div>
                    <Table aria-labelledby='tableTitle'>
                        <SortableTableHead onRequestSort={this.handleRequestSort} oreder={order} orderBy={orderBy} rowCount={data.length}/>
                        <TableBody>
                            {/*{data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return(
                                    <TableRow key={n.id}>
                                        <TableCell>
                                            <DialogWindowTable fields={n}/>
                                        </TableCell>
                                        <TableCell>{n.measure}</TableCell>
                                        <TableCell numeric>{n.usage}</TableCell>
                                        <TableCell numeric>{n.min}</TableCell>
                                        <TableCell numeric>{n.max}</TableCell>
                                    </TableRow>
                                );
                            })}*/}
                            {dataFromNode.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                                    return(
                                    <TableRow key={n.id}>
                                        <TableCell>
                                            <DialogWindowTable fields={n}/>
                                        </TableCell>
                                        <TableCell>{n.measure}</TableCell>
                                        <TableCell numeric>{n.usage}</TableCell>
                                        <TableCell numeric>{n.min}</TableCell>
                                        <TableCell numeric>{n.max}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    component='div'
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{'aria-label' : 'Next Page'}}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                />
            </Paper>
        );
    };

}

export default SortableTable;