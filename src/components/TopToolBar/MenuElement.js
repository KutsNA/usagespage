import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVertIcon from '@material-ui/icons/MoreVert';



class MenuElement extends Component{

    state={
        isOpen : null,
    };

    handleClick = event => {
        this.setState({isOpen: event.currentTarget});
    };

    handleClose = () => {
        this.setState({isOpen : null});
    };

    render(){
        var counter = 0;
        const menuItems = this.props.fields;
        const menuList = menuItems.map(item => {
                                                  counter++;
                                                  return(<MenuItem key={counter} onClick={this.handleClose}>{item}</MenuItem>);
        });
        return(
            <div>
                <Button
                    size='small'
                    variant='outlined'
                    color='inherit'
                    aria-owns={this.state.isOpen? 'simple-menu' : null}
                    aria-haspopup='true'
                    onClick={this.handleClick}>
                    {this.props.name}
                    <MoreVertIcon style={{fontSize: 16}}/>
                </Button>
                <Menu id='sample-menu' anchorEl={this.state.isOpen} open={Boolean(this.state.isOpen)} onClose={this.handleClose}>
                    {menuList}
                </Menu>
            </div>
        );
    };
}

export default MenuElement;