import React, {Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import ToolBar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import MenuElement from './MenuElement';

class TopManuBar extends Component{
    render(){
        const usagesFields= ['Usages Grid', 'History report'];
        const staticDataFields =['Groups', 'Issuer Rating', 'Hierarchy', 'Reference Data', 'Market Data', 'Measures', 'FX Conversion Rules'];
        const permissionsFields = ['Users', 'Roles'];
        return(
            <AppBar position='static' color='inherit'>
                <ToolBar>
                    <Button size='small' variant='outlined' color='inherit'>Atlas</Button>
                    <Button size='small' variant='outlined' color='inherit'>Limits</Button>
                    <Button size='small' variant='outlined' color='inherit'>Templates</Button>
                    <Button size='small' variant='outlined' color='inherit'>Reports</Button>
                    <Button size='small' variant='outlined' color='inherit'>Calculator</Button>
                    <Button size='small' variant='outlined' color='inherit'>Analytics</Button>
                    <Button size='small' variant='outlined' color='inherit'>RealTime</Button>
                    <Button size='small' variant='outlined' color='inherit'>IntradayDashboard</Button>
                    <Button size='small' variant='outlined' color='inherit'>Audit</Button>
                    <Button size='small' variant='outlined' color='inherit'>LiquidityDashboard</Button>
                    <MenuElement name='Usages' fields={usagesFields}/>
                    <MenuElement name='Static Data' fields={staticDataFields}/>
                    <MenuElement name='Permissions' fields={permissionsFields}/>
                    <Button size='small' variant='raised' color='secondary' style={{position: 'absolute', right: '15px'}}>SignOut</Button>
                </ToolBar>
            </AppBar>
        );
    };
}

export default TopManuBar