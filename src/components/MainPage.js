import React, {Component} from 'react';
import '../styles/App.css';
import Button from "@material-ui/core/Button";
import MultipleSelect from './DateAndMeasureSelectionBar/MultipleMeasureSelection'
import Calender from './DateAndMeasureSelectionBar/Calender';
import TableContainer from './TableContainer/Table';
import TopMenuToolBar from './TopToolBar/TopMenuBar';
import {findUsagesByMeasure, findUsagesByDate} from "../usages/api";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import measures from './namesOfMeasures';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: new Date().toLocaleDateString(),
            selectedMeasures: [],
            usages: [],
            isShown: false
        }
    };

    onMeasuresSelected = (selectedMeasures) => {
        this.setState({selectedMeasures});
    };

    onDateSelected = (selectedDate) => {
        this.setState({selectedDate});
    };

    requestUsages = () => {
        const date = this.state.selectedDate;

        if (date) {
            const measure = this.state.selectedMeasures;
            if (measure) {
                findUsagesByMeasure(date, measure)
                    .then(usages => this.setState({usages}));
            } else {
                findUsagesByDate(date)
                    .then(usages => this.setState({usages}));
            }
        }
    };

    showUsages = () => {
        this.setState(prev => ({isShown: !prev.isShown}));
        this.requestUsages(); //turn on when UI will be connected to DB
    };

    render() {
        const arr = ['1','2','3','4','5'];
        return (
            <div>
                <Grid container spacing={0}>
                    <Grid item xs={12} style={{paddingBottom: '8px'}}>
                        <TopMenuToolBar/>
                    </Grid>
                    <Grid item xs={12} style={{paddingBottom: '8px'}}>
                        <Paper>
                            <Grid item container xs={12} direction='row'>
                                <Grid item xs={2}>
                                    <Calender onDateSelected={this.onDateSelected}/>
                                </Grid>
                                <Grid item xs={6} >
                                    <MultipleSelect measure={measures}
                                                    selectedMeasures={this.state.selectedMeasures}
                                                    onMeasuresSelected={this.onMeasuresSelected}/>
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item>
                        <Button variant="raised" color="primary"
                                onClick={this.showUsages}>Show usages</Button>
                    </Grid>
                    <Grid item>measures
                        <Button variant="raised" color="primary">Export to csv</Button>
                    </Grid>
                    <Grid item xs={12}>
                        {this.state.isShown && <TableContainer usages={arr}/>}
                    </Grid>
                </Grid>
            </div>
        );
    };
}

export default MainPage;