import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};


class MultipleSelect extends React.Component {

    handleChange = event => {
        this.props.onMeasuresSelected(event.target.value);
    };

    clearMeasures = () => {
        this.props.onMeasuresSelected([]);
    };

    render() {
        const {classes} = this.props;
        const names = this.props.measure;
        const selectedMeasures = this.props.selectedMeasures;
        return (
            <div>
                <FormControl className={classes.formControl}>
                    <InputLabel>Measures</InputLabel>
                    <Select
                        multiple
                        value={selectedMeasures}
                        onChange={this.handleChange}
                        input={<Input id="select-multiple-checkbox"/>}
                        renderValue={selected => selected.join(', ')}
                        MenuProps={MenuProps}
                        label="Measures"
                    >
                        {names.map(name => (
                            <MenuItem key={name} value={name}>
                                <Checkbox checked={selectedMeasures.indexOf(name) > -1}/>
                                <ListItemText primary={name}/>
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <Button variant='raised' color='primary' onClick={this.clearMeasures}>Drop measures</Button>
            </div>
        );
    }
}

MultipleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(MultipleSelect);
