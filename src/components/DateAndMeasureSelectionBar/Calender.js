import React, {PureComponent} from 'react';
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import DatePicker from 'material-ui-pickers/DatePicker';
import {withStyles} from '@material-ui/core/styles';
import {KeyboardArrowRight, KeyboardArrowLeft} from '@material-ui/icons';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: 300,
    },
});

class Calender extends PureComponent {
    state = {
        selectedDate: new Date(),
    };

    handleDateChange = (date) => {
        this.setState({selectedDate: date});
        this.props.onDateSelected(date);
    };

    render() {
        const {selectedDate} = this.state;
        const {classes} = this.props;
        return (
            <div className={classes.formControl}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DatePicker label="Date"
                                showTodayButton
                                value={selectedDate}
                                onChange={this.handleDateChange}
                                leftArrowIcon={<KeyboardArrowLeft/>}
                                rightArrowIcon={<KeyboardArrowRight/>}
                    />
                </MuiPickersUtilsProvider>
            </div>
        );
    }
}

export default withStyles(styles)(Calender);